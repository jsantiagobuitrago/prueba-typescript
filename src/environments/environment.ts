// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig:{
    apiKey: "AIzaSyA0X7-yIEY5D8iNS0CUIiedL31tLq7wJ-c",
    authDomain: "ventafacil-sas.firebaseapp.com",
    projectId: "ventafacil-sas",
    storageBucket: "ventafacil-sas.appspot.com",
    messagingSenderId: "799124446395",
    appId: "1:799124446395:web:1ef1296c17c3febe1c1d9e"
  }
};



/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
