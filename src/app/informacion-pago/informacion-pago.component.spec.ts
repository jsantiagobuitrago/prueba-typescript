import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { InformacionPagoComponent } from './informacion-pago.component';

describe('InformacionPagoComponent', () => {
  let component: InformacionPagoComponent;
  let fixture: ComponentFixture<InformacionPagoComponent>;


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      declarations: [InformacionPagoComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(InformacionPagoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
