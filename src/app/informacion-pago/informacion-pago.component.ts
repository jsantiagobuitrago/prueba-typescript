import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import Swal from 'sweetalert2'

@Component({
  selector: 'app-informacion-pago',
  templateUrl: './informacion-pago.component.html',
  styleUrls: ['./informacion-pago.component.css']
})
export class InformacionPagoComponent implements OnInit {

  formularioPago: FormGroup;

  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute) {
    this.formularioPago = this.formBuilder.group({
      nombre: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      telefono: ['', Validators.required],
      direccion: ['', Validators.required],
      numeroTarjeta: ['', [Validators.required, Validators.minLength(16), Validators.maxLength(16)]],
      fechaVencimiento: ['', Validators.required],
      codigoSeguridad: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(3)]]
    });
  }

  finalizarCompra() {
    if (this.formularioPago.valid) {
      Swal.fire({
        title: '¡Bien!',
        text: 'Compra finalizada',
        icon: 'success',
        confirmButtonText: 'Ok'
      })
      sessionStorage.removeItem('cart');
      
    } else {
      Swal.fire({
        title: '¡Error!',
        text: 'Hay un error en el formulario',
        icon: 'error',
        confirmButtonText: 'Ok'
      })
      if (this.formularioPago.get('nombre')!.invalid) {
        Swal.fire({
          title: '¡Error!',
          text: 'El campo de Nombre es invalido',
          icon: 'error',
          confirmButtonText: 'Ok'
        })
      }
      if (this.formularioPago.get('email')!.invalid) {
        Swal.fire({
          title: '¡Error!',
          text: 'El campo de Email es invalido',
          icon: 'error',
          confirmButtonText: 'Ok'
        })
      }
      if (this.formularioPago.get('telefono')!.invalid) {
        Swal.fire({
          title: '¡Error!',
          text: 'El campo de Telefono es invalido',
          icon: 'error',
          confirmButtonText: 'Ok'
        })
      }
      if (this.formularioPago.get('direccion')!.invalid) {
        Swal.fire({
          title: '¡Error!',
          text: 'El campo de Direccion es invalido',
          icon: 'error',
          confirmButtonText: 'Ok'
        })
      }
      if (this.formularioPago.get('numeroTarjeta')!.invalid) {
        Swal.fire({
          title: '¡Error!',
          text: 'El campo de Numero de Tarjeta es invalido (Debe contener 16 numeros)',
          icon: 'error',
          confirmButtonText: 'Ok'
        })
      }
      if (this.formularioPago.get('fechaVencimiento')!.invalid) {
        Swal.fire({
          title: '¡Error!',
          text: 'El campo de Fechad e Vencimiento es invalido',
          icon: 'error',
          confirmButtonText: 'Ok'
        })
      }
      if (this.formularioPago.get('codigoSeguridad')!.invalid) {
        Swal.fire({
          title: '¡Error!',
          text: 'El campo de Codigo de Seguridad es invalido',
          icon: 'error',
          confirmButtonText: 'Ok'
        })
      }

    }
  }

  ngOnInit(): void {
  }

}
