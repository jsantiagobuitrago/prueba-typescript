import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { CatalogoArticulosComponent } from './components/catalogo-articulos/productos.component';
import { AngularFireModule } from '@angular/fire/compat';
import { AngularFirestoreModule } from '@angular/fire/compat/firestore';
import { RouterModule, Routes } from '@angular/router';
import { DetalleArticuloComponent } from './components/detalle-articulo/detalle-articulo.component';
import { AgregarModificarArticulosComponent } from './components/agregar-modificar-articulos/agregar-modificar-articulos.component';
import { FormsModule } from '@angular/forms';
import { InformacionPagoComponent } from './informacion-pago/informacion-pago.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CarritoGuardService } from './services/carrito-guard.service';
import { CommonModule } from '@angular/common';
import { RouterTestingModule } from '@angular/router/testing';
import { FIREBASE_OPTIONS } from '@angular/fire/compat';
import { environment } from 'src/environments/environment';
import { initializeApp, provideFirebaseApp } from "@angular/fire/app";


const routes: Routes = [


  { path: '', component: CatalogoArticulosComponent },
  { path: 'productos', redirectTo: "" },
  { path: 'detalle-articulo/:id', component: DetalleArticuloComponent },
  { path: 'ver-carrito', component: AgregarModificarArticulosComponent },
  { path: 'informacion-pago', component: InformacionPagoComponent, canActivate: [CarritoGuardService] }
];



@NgModule({
  declarations: [
    AppComponent,
    CatalogoArticulosComponent,
    DetalleArticuloComponent,
    AgregarModificarArticulosComponent,
    InformacionPagoComponent
  ],
  imports: [
    BrowserModule,
   // AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireModule,
    AngularFirestoreModule,
    RouterModule.forRoot(routes),
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    RouterModule,
    RouterTestingModule,
    provideFirebaseApp(() => initializeApp(environment.firebaseConfig)),
  ], 
  exports: [
    RouterModule
  ],
  providers: [CarritoGuardService,
    { provide: FIREBASE_OPTIONS, useValue: environment.firebaseConfig }],
  
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule { }


