import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore } from '@angular/fire/compat/firestore';

@Injectable({
  providedIn: 'root'
})
export class ArticuloService {

  private articulos: Observable<any[]>;

  constructor(private firestore: AngularFirestore) {
    this.articulos = firestore.collection('articulos').valueChanges();
    
  }

  getArticulos(): Observable<any[]> {
    return this.articulos;
  }

  getArticulo(id: any): Observable<any> {
    return this.firestore.doc<any>(`articulos/${id}`).valueChanges();
  }

}
