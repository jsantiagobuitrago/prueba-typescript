import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { SessionStorageService } from 'angular-web-storage';

@Injectable({
  providedIn: 'root'
})
export class CarritoGuardService {

  constructor(private sessionStorage: SessionStorageService, private router: Router) {}

  canActivate(): boolean {
    const cart = this.sessionStorage.get('cart');
    if (cart && cart.length > 0) {
      return true;
    } else {
      this.router.navigate(['/']);
      return false;
    }
  }
}
