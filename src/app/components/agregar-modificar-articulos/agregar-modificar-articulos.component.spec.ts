import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AngularFireModule } from '@angular/fire/compat';
import { AngularFirestore, AngularFirestoreModule } from '@angular/fire/compat/firestore';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { environment } from 'src/environments/environment';

import { AgregarModificarArticulosComponent } from './agregar-modificar-articulos.component';

describe('AgregarModificarArticulosComponent', () => {
  let component: AgregarModificarArticulosComponent;
  let fixture: ComponentFixture<AgregarModificarArticulosComponent>;

  describe('AgregarModificarArticulosComponent', () => {
    let component: AgregarModificarArticulosComponent;
    let fixture: ComponentFixture<AgregarModificarArticulosComponent>;
    let activatedRoute: ActivatedRoute;

    beforeEach(async(() => {
      TestBed.configureTestingModule({
        imports: [RouterTestingModule,
          AngularFireModule.initializeApp(environment.firebaseConfig),
          AngularFirestoreModule],
        declarations: [ AgregarModificarArticulosComponent,
          { provide: ActivatedRoute, useValue: activatedRoute, AngularFirestore }]
      })
      .compileComponents();
      fixture = TestBed.createComponent(AgregarModificarArticulosComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    }));
  
    beforeEach(() => {
      fixture = TestBed.createComponent(AgregarModificarArticulosComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });
  
    it('should create', () => {
      expect(component).toBeTruthy();
    });
  
    it('should render title in a h1 tag', () => {
      const compiled = fixture.nativeElement;
      expect(compiled.querySelector('h1').textContent).toContain('Welcome to agregar-modificar-articulos!');
    });
  });
});
