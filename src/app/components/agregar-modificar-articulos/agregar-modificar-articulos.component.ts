import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/interfaces/product';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-agregar-modificar-articulos',
  templateUrl: './agregar-modificar-articulos.component.html',
  styleUrls: ['./agregar-modificar-articulos.component.css']
})
export class AgregarModificarArticulosComponent implements OnInit {



  articulos: Product[] | undefined 
  articulo: Product | undefined
  articulos_unicos: Product[] = []

  constructor(private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    if (sessionStorage.getItem('cart')) {
      this.articulos = JSON.parse(sessionStorage.getItem('cart')!)['_value']
      for (let articulo of this.articulos!) {
        this.articulo = this.articulos_unicos.find(p => p.id === articulo.id)
        if (this.articulo) {
          this.articulo.cantidad++
        } else {
          this.articulos_unicos.push(Object.assign({}, articulo, { cantidad: 1 }))
        }
      }
    }
    else {
      
    }

    console.log(this.articulos_unicos)
  }

  eliminarArticulo(articulo: Product) {
    let index = this.articulos_unicos.indexOf(articulo);
    if (index > -1) {
      this.articulos_unicos.splice(index, 1);
    }
  }


  calcularTotal() {
    let total = 0;
    this.articulos_unicos.forEach(articulo => {
      total += articulo.precio * articulo.cantidad;
    });
    return total;
  }

  
  volver() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }
}
