import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/compat/firestore';



@Component({
  selector: 'app-catalogo-articulos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.css']
})
export class CatalogoArticulosComponent implements OnInit {

  constructor(private firestore: AngularFirestore) { }

  articulos: Observable<any[]> | undefined;

  ngOnInit() {
    this.articulos = this.firestore.collection('articulos').valueChanges({idField: 'idDoc'});
    

    
  }


}
