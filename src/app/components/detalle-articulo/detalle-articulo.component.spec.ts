import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { DetalleArticuloComponent } from './detalle-articulo.component';
import { RouterModule } from '@angular/router';
import { AngularFireModule } from '@angular/fire/compat';
import { AngularFirestore, AngularFirestoreModule } from '@angular/fire/compat/firestore';
import { environment } from 'src/environments/environment';
import { ArticuloService } from 'src/app/services/articulo-service.service';
import { of } from 'rxjs';

describe('DetalleArticuloComponent', () => {
  let component: DetalleArticuloComponent;
  let fixture: ComponentFixture<DetalleArticuloComponent>;
  let activatedRoute: ActivatedRoute;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ RouterModule,
        AngularFireModule.initializeApp(environment.firebaseConfig),
        AngularFirestoreModule ],
      declarations: [ DetalleArticuloComponent ],
      providers: [ { provide: ActivatedRoute, useValue: activatedRoute, AngularFirestore } ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DetalleArticuloComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('ArticuloServiceService', () => {
  let service: ArticuloService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ArticuloService)
    fixture = TestBed.createComponent(DetalleArticuloComponent);
    component = fixture.componentInstance;
    activatedRoute = TestBed.get(ActivatedRoute);
    activatedRoute.params = of({ id: '123' });
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
 


});