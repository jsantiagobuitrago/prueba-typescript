import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map, Subscription, tap } from 'rxjs';
import { ArticuloService } from '../../services/articulo-service.service';
import { SessionStorageService } from 'angular-web-storage';
import { Product } from 'src/app/interfaces/product';
import Swal from 'sweetalert2'
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/compat/firestore';


@Component({
  selector: 'app-detalle-articulo',
  templateUrl: './detalle-articulo.component.html',
  styleUrls: ['./detalle-articulo.component.css']
})
export class DetalleArticuloComponent implements OnInit {

  articulo: any;
  id: number | undefined;
  private routeSub: Subscription = new Subscription();

  constructor(
    private route: ActivatedRoute,
    private articuloService: ArticuloService,
    //private carritoService: CarritoService
    private sessionStorage: SessionStorageService
  ) { }

  ngOnInit() {
    this.routeSub = this.route.params.subscribe(params => {
      this.id = params['id'];
      
      this.articuloService.getArticulo(this.id)
      .subscribe((data: any) => {
        this.articulo = data;
      });

    });
  }

  ngOnDestroy() {
    this.routeSub.unsubscribe();
  }

  agregarArticulo(articulo: any) {
    console.log(articulo)
    let cart = this.sessionStorage.get('cart');
    if (!cart) {
      cart = [];
    }
    cart.push(articulo);
    this.sessionStorage.set('cart', cart);
    Swal.fire({
      title: '¡Agregado!',
      text: articulo.nombre + " agregado al carrito",
      icon: 'success',
      confirmButtonText: 'Ok'
    })
   // this.carritoService.agregarArticulo(articulo);
   
  }
}
